package com.gslab.gsblog.controllers;

import com.gslab.gsblog.dao.Blog;
import com.gslab.gsblog.dao.JdbcBlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BlogsContoller {
    @Autowired
    JdbcBlogRepository jdbcBlogRepository;

    @GetMapping("/blogs")
    public List<Blog> getBlogs() {
        return jdbcBlogRepository.getAllBlogs();
    }

    @GetMapping("/blogs/{id}")
    public Blog getAthorByAuthorId(@PathVariable int id) {
        return jdbcBlogRepository.getBlogByBlogId(id).get();
    }

}
