package com.gslab.gsblog.controllers;

import com.gslab.gsblog.dao.Author;
import com.gslab.gsblog.dao.JdbcAuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthorsController {
    @Autowired
    JdbcAuthorRepository jdbcAuthorRepository;

    @GetMapping("/authors")
    public List<Author> getAllAuthors() {
        return jdbcAuthorRepository.getAllAuthors();
    }

    @GetMapping("/authors/{id}")
    public Author getAthorByAuthorId(@PathVariable int id) {
        return jdbcAuthorRepository.getAuthorById(id).get();
    }
}
