package com.gslab.gsblog.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class HomeController {

    //TODO: Remove dummy index page
    @GetMapping("/")
    public String index() {
        return "Hello";
    }

}
