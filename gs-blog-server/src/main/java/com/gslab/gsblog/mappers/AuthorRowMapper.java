package com.gslab.gsblog.mappers;

import com.gslab.gsblog.dao.Author;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorRowMapper implements RowMapper<Author> {
    @Override
    public Author mapRow(ResultSet rs, int rowNum) throws SQLException {
        Author author = new Author();
        author.setAuthorId(rs.getInt("authorid"));
        author.setAuthorName(rs.getString("authorname"));
        return author;
    }
}
