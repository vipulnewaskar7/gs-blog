package com.gslab.gsblog.mappers;

import com.gslab.gsblog.dao.Blog;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BlogRowMapper implements RowMapper<Blog> {

    @Override
    public Blog mapRow(ResultSet rs, int rowNum) throws SQLException {
        Blog blog = new Blog();
        blog.setBlogId(rs.getInt("blogid"));
        blog.setAuthorId(rs.getInt("authorid"));
        blog.setContent(rs.getString("content"));
        return blog;
    }
}
