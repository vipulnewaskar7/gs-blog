package com.gslab.gsblog.util;

import com.gslab.gsblog.dao.Author;
import com.gslab.gsblog.dao.Blog;
import com.gslab.gsblog.dao.IBlog;

import java.util.ArrayList;
import java.util.List;

public class TempData {
    public static Blog blog = new Blog();
    public static Author author = new Author();
    public static List<IBlog> allBlogs = new ArrayList<>();

    static {
        author.setAuthorId(6);
        author.setAuthorName("DUMMY");
        blog.setContent("Some Blog Content");
        blog.setAuthorId(6);
        allBlogs.add(blog);
    }


}
