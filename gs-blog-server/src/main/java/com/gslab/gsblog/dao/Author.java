package com.gslab.gsblog.dao;

public class Author implements IAuthor {
    private int authorid;
    private String authorname;

    public int getAuthorId() {
        return authorid;
    }

    public void setAuthorId(int authorid) {
        this.authorid = authorid;
    }

    public String getAuthorName() {
        return authorname;
    }

    public void setAuthorName(String authorname) {
        this.authorname = authorname;
    }


}
