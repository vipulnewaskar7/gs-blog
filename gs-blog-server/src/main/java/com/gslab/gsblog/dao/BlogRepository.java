package com.gslab.gsblog.dao;

import java.util.List;
import java.util.Optional;

public interface BlogRepository {
    List<Blog> getAllBlogs();

    List<Blog> getBlogsByAuthorId(int authorid);

    Optional<Blog> getBlogByBlogId(int blogid);
}
