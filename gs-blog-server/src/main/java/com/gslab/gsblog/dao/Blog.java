package com.gslab.gsblog.dao;

public class Blog implements IBlog {
    private int blogid;
    private int authorid;
    private String content;

    public int getBlogId() {
        return blogid;
    }

    public void setBlogId(int blogid) {
        this.blogid = blogid;
    }

    public int getAuthorId() {
        return authorid;
    }

    public void setAuthorId(int authorid) {
        this.authorid = authorid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
