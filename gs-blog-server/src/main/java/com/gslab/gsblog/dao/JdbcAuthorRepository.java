package com.gslab.gsblog.dao;

import com.gslab.gsblog.mappers.AuthorRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class JdbcAuthorRepository implements AuthorRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Author> getAllAuthors() {
        return jdbcTemplate.query("SELECT authorid, authorname FROM authors", new AuthorRowMapper());
    }

    @Override
    public Optional<Author> getAuthorById(int authorid) {
        String query = "SELECT * FROM authors where authorid=" + authorid;
        return jdbcTemplate.queryForObject(query, (rs, rowNum) -> {
            Author author = new Author();
            author.setAuthorId(rs.getInt("authorid"));
            author.setAuthorName(rs.getString("authorname"));
            return Optional.of(author);
        });
    }
}
