package com.gslab.gsblog.dao;

import com.gslab.gsblog.mappers.BlogRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class JdbcBlogRepository implements BlogRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Blog> getAllBlogs() {
        return jdbcTemplate.query("SELECT blogid, authorid, content FROM blogs", new BlogRowMapper());

    }

    @Override
    public List<Blog> getBlogsByAuthorId(int authorid) {
        return null;
    }

    @Override
    public Optional<Blog> getBlogByBlogId(int blogid) {
        String query = "SELECT * FROM blogs where blogid=" + blogid;
        return jdbcTemplate.queryForObject(query, (rs, rowNum) -> {
            Blog blog = new Blog();
            blog.setBlogId(rs.getInt("blogid"));
            blog.setAuthorId(rs.getInt("authorid"));
            blog.setContent(rs.getString("content"));
            return Optional.of(blog);
        });
    }
}
