package com.gslab.gsblog.dao;

import java.util.List;
import java.util.Optional;

public interface AuthorRepository {
    List<Author> getAllAuthors();

    Optional<Author> getAuthorById(int id);
}
