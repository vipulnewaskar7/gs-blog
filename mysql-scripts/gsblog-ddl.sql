CREATE TABLE `authors` (
  `authorid` int NOT NULL AUTO_INCREMENT,
  `authorname` varchar(100) NOT NULL,
  PRIMARY KEY (`authorid`)
);

CREATE TABLE `blogs` (
  `blogid` int NOT NULL AUTO_INCREMENT,
  `authorid` int NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`blogid`),
  KEY `blogs_FK` (`authorid`),
  CONSTRAINT `blogs_FK` FOREIGN KEY (`authorid`) REFERENCES `authors` (`authorid`)
);

INSERT INTO gsblog.authors (authorname) VALUES 
('vipul.newaskar')
,('sima.ubale')
,('ashok.shendage')
;

INSERT INTO gsblog.blogs (authorid,content) VALUES 
(1,'Hello From Vipul')
,(2,'Hi From Sima')
,(2,'Hi Again from Sima')
,(3,'Hi, It''s Ahsok Here')
;